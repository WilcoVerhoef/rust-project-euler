use std::{
    iter::successors,
    ops::{Add, Div},
};

use num::{integer::gcd, BigInt, BigUint, ToPrimitive};

struct Rational {
    numerator: BigInt,
    denumerator: BigUint,
}

impl Rational {
    fn reduce(self) -> Self {
        let gcd = &gcd(self.numerator.clone(), self.denumerator.clone().into());
        Rational {
            numerator: self.numerator / gcd,
            denumerator: self.denumerator / gcd.to_biguint().unwrap(),
        }
    }
}

impl Add for Rational {
    type Output = Rational;

    fn add(self, rhs: Self) -> Self::Output {
        Rational {
            numerator: self.numerator * BigInt::from(rhs.denumerator.clone())
                + rhs.numerator * BigInt::from(self.denumerator.clone()),
            denumerator: self.denumerator * rhs.denumerator,
        }
        .reduce()
    }
}

impl Div for Rational {
    type Output = Rational;

    fn div(self, rhs: Self) -> Self::Output {
        use num::bigint::Sign::*;
        Rational {
            numerator: self.numerator
                * BigInt::from(rhs.denumerator)
                * match rhs.numerator.sign() {
                    Minus => -1,
                    NoSign => panic!("attempt to divide `Rational` by zero"),
                    Plus => 1,
                },
            denumerator: self.denumerator * num::abs(rhs.numerator).to_biguint().unwrap(),
        }
        .reduce()
    }
}

impl From<i64> for Rational {
    fn from(value: i64) -> Self {
        Rational {
            numerator: BigInt::from(value),
            denumerator: BigUint::from(1_u64),
        }
    }
}

fn digits(n: BigInt) -> impl Iterator<Item = i32> {
    successors(Some(n), |n| {
        Some(n / 10_u32).filter(|n| n > &BigInt::from(0_u32))
    })
    .map(|n| (n % 10_u32).to_i32().unwrap())
}

fn main() {
    let sequence = (1..=33).flat_map(|k| [1, 2 * k, 1]).map(Rational::from);
    let convergent = Rational::from(2)
        + sequence.rfold(Rational::from(0), |acc, n| Rational::from(1) / (n + acc));
    println!("{}", digits(convergent.numerator).sum::<i32>());
}

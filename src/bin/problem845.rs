use std::iter::successors;

use num::{BigInt, ToPrimitive};

fn main() {
    let a = pds(&BigInt::from(61));
    println!("D(61) = {}", a);
    assert_eq!(a, BigInt::from(157));

    let b = pds(&BigInt::from(10).pow(8));
    println!("D(10^8) = {}", b);
    assert_eq!(b, BigInt::from(403539364));

    println!("D(10^16) = {}", pds(&BigInt::from(10).pow(16)));
}

fn choose(n: i32, k: i32) -> BigInt {
    if !(0 <= k && k <= n) {
        return BigInt::from(0_u32);
    }

    let with: BigInt = ((k + 1).max(n - k + 1)..=n).product();
    let out: BigInt = (2..=k.min(n - k)).product();
    with / out
}

fn primes() -> impl Iterator<Item = i32> {
    (2..).filter(|&p| (2..p).all(|d| p % d != 0))
}

fn digits(n: BigInt) -> Vec<i32> {
    successors(Some(n), |n| {
        Some(n / 10_u32).filter(|n| n > &BigInt::from(0_u32))
    })
    .map(|n| ((n % 10) as BigInt).to_i32().unwrap())
    .collect()
}

fn pds_below_power_of_10_offset(n: i32, o: i32) -> BigInt {
    primes()
        .map(|p| p - o)
        .take_while(|&p| p <= 10 * n)
        .map(|p| {
            if p == 0 {
                return BigInt::from(1);
            }
            (0..=n)
                .map(move |i| {
                    let r =
                        (-1_i32).pow(i as u32) * choose(n, i) * choose(p + n - 1 - 10 * i, n - 1);
                    r
                })
                .sum::<BigInt>()
        })
        .sum()
}

fn pds_below(n: &BigInt) -> BigInt {
    let (_, total) =
        digits(n.clone())
            .iter()
            .enumerate()
            .rev()
            .fold((0, BigInt::from(0)), |(o, t), (e, n)| {
                (
                    o + n,
                    t + (o..o + n)
                        .map(|o| pds_below_power_of_10_offset(e as i32, o))
                        .sum::<BigInt>(),
                )
            });
    return total;
}

fn log_search<P>(predicate: P) -> BigInt
where
    P: Fn(&BigInt) -> bool,
{
    if predicate(&BigInt::from(0)) {
        todo!("Searching for a negative value...")
    }

    // Logarithmic Search

    let mut end = BigInt::from(1);
    while !predicate(&end) {
        end *= 2;
    }

    // Binary Search

    let mut start: BigInt = end.clone() / 2;
    while &start + 1 < end {
        let mid = (&start + &end) / 2;
        if predicate(&mid) {
            end = mid;
        } else {
            start = mid;
        }
    }

    return end;
}

fn pds(n: &BigInt) -> BigInt {
    log_search(|b| pds_below(b) >= *n) - 1
}

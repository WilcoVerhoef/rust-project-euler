use na::{Matrix2, matrix, Vector2, vector};

extern crate nalgebra as na;

fn even_fibonacci_numbers(n: i32) -> i32 {
    let fib_mat: Matrix2<i32> = matrix![0, 1; 1, 1].pow(3);

    let mut fib_vec: Vector2<i32> = vector![0, 1];
    let mut sum: i32 = 0;

    while fib_vec[0] <= n {
        sum += fib_vec[0];
        fib_vec = fib_mat * fib_vec;
    }

    sum
}

fn main() {
    println!("{}", even_fibonacci_numbers(4_000_000));
}

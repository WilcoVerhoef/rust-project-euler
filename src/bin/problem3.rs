fn largest_prime_factor(mut n: u64) -> u64 {
    let mut d = 2;

    while n != 1 {
        if n % d == 0 {
            n /= d;
        } else {
            d += 1;
        }
    }

    return d;
}

fn main() {
    println!("{}", largest_prime_factor(600851475143));
}

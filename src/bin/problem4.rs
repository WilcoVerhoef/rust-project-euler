fn digits(n: i32) -> Vec<i32> {
    let mut n = n;
    let mut digits = Vec::new();

    while n > 0 {
        digits.push(n % 10);
        n /= 10;
    }

    digits
}

fn is_palindrome(n: i32) -> bool {
    let digits = digits(n);

    for i in 0..digits.len() / 2 {
        if digits[i] != digits[digits.len() - i - 1] {
            return false;
        }
    }

    true
}

fn largest_palindrome_product(n: i32) -> i32 {
    let mut largest = 0;
    
    for x in 0..n {
        for y in 0..n {
            if x * y > largest && is_palindrome(x * y) {
                largest = x * y;
            }
        }
    }

    largest
}

fn main() {
    println!("{}", largest_palindrome_product(1000));
}

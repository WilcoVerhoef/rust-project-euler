/*
"information theory"

lets just assume that it works...

nope it doesn't.

==================

a tactic that might work for `h`:

- first determine plate (2)
- then determine bean (ceil . 2log $ maximum [a,b,c])
- ISN'T ENOUGH!


==================

Lower bound:    ceil . 2log $ maximum [a,b,c]
Upper bound:    ceil . 2log $ maximum [a,b,c] + 2


Cases:

h(0,0,2^k) = k
h(0,1,2^k) = k + 1
h(0,1,2^k-1) = k + 1


Je moet eigenlijk stennen weghalen totdat er één overblijft.

just say that a <= b <= c

h(a,b,c) =
    you want to check `2^k` beans of c, with k = h(a,b,c - 2^k)

========

reverse the question. How many beans in a,b,c with at most `k` guesses?
- first guess: take 2^(k-1) from C
-

h(a,b,c) = -1:
    h(0,0,0);

h(a,b,c) = 0:
    h(0,0,1);

h(a,b,c) = 1:
    h(0,0,2)
    h(0,1,1)

h(a,b,c) = 2:
    h(0,0,4)
    h(0,1,3)
    h(0,2,2)
    h(1,1,2)
  so always, take away 2 from c, and arrive at a h(a,b,c) = 1
  in other words, we can generate these by adding 2 to each sol to h(a,b,c) = 1
  ->
    h(0,0,4)
    h(0,2,2)
    h(0,1,3)
    h(1,1,2)

h(a,b,c) = 3:
    h(0,0,8)
    h(0,1,7)
    h(0,2,6)

so in general, an h(a,b,c) can be solved in k steps if:
  - it can be divided in groups of at most 1, 1, 2, 4, 8, ... 2^(k-1)

hmmmm, what if we look at the binary representation of a,b,c:

    00101110101
    01110101001
    10100010011

Work from the least sigdig to the most sigdig, try to fill them all.
if for a given dig all of a,b,c are zero: get a 1 from the next slot.
hmmm, so that would mean that we can just count the total number of 1's??
holy shit that would be awesome!


h(a,b,c) =
*/

fn h(a: u64, b: u64, c: u64) -> u64 {
    println!("h({},{},{})", a, b, c);
    // (a.count_ones() + b.count_ones() + c.count_ones() - 1) as u64
    (a + b + c).next_power_of_two().ilog2() as u64
}

fn hh(nn: u64) -> u64 {
    let mut sum = 0;
    for a in 0..=nn {
        for b in 0..(nn - a) {
            for c in 0..(nn - a - b) {
                if a + b + c == 0 {
                    continue;
                }
                sum += h(a, b, c);
            }
        }
    }
    return sum;
}

fn main() {
    // assert_eq!(h(1, 2, 3), 3);
    // assert_eq!(h(2, 3, 3), 4);
    // assert_eq!(hh(6), 203);
    // assert_eq!(hh(20), 7718);
    let r19 = 1_000_000_000_u64;
    for i in 1..r19 {
        if i % 10000000 == 0 {
            println!("{} ({}%)", i, i as f64 / r19 as f64);
        }
    }
}

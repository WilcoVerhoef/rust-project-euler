fn is_multiple_of_3_or_5(n: &i32) -> bool {
    n % 3 * n % 5 == 0
}

fn multiples_of_3_and_5(n: i32) -> i32 {
    (1..n)
    .filter(is_multiple_of_3_or_5)
    .sum()
}

fn main() {
    println!("{}", multiples_of_3_and_5(1000));
}

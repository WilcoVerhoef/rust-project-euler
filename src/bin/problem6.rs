fn sum_of_squares(n: i32) -> i32 {
    (1..n+1)
    .map(|x| x.pow(2))
    .sum()
}

fn square_of_sums(n: i32) -> i32 {
    (1..n+1).sum::<i32>().pow(2)
}

fn sum_square_difference(n: i32) -> i32 {
    square_of_sums(n) - sum_of_squares(n)
}

fn main() {
    println!("{}", sum_square_difference(100));
}

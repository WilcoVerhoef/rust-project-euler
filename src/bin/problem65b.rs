use num::{integer::div_rem, one, zero, BigInt, ToPrimitive};
use std::{
    iter::{from_fn, once},
    mem::take,
};

/// Returns the decimal digits of a number.
fn digits(mut n: BigInt) -> impl Iterator<Item = u32> {
    from_fn(move || {
        if n == zero() {
            None
        } else {
            let rem: BigInt;
            (n, rem) = div_rem(take(&mut n), BigInt::from(10));
            rem.to_u32()
        }
    })
}

fn main() {
    // sequence = [2, 1, 2, 1, 1, 4, 1, ..., 1, 66, 1]
    let sequence = once(2).chain((1..=33).flat_map(|k| [1, 2 * k, 1]));

    // computed continued fraction as a rational number (numerator ÷ denominator)
    let (num, _den) = sequence.rfold((one(), zero()), |(num, den), k| (den + k * &num, num));

    println!("{}", digits(num).sum::<u32>());
}

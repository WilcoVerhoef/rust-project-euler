use std::iter::successors;

type Num = i64;

fn main() {
    assert_eq!(ff(2), 1);
    assert_eq!(ff(3), 4);
    assert_eq!(ff(7), 3);
    assert_eq!(ff(42), 5);
    assert_eq!(ff(89), 12598);

    let a = (1..=100).map(|n| ff(n)).sum::<Num>();
    assert_eq!(a, Num::from(11363107));
    println!("sum 1..100 {{ f(n) / n }} = {}", a);

    let b = (1..=10000).map(|n| ff(n)).sum::<Num>();
    println!("sum 1..10000 {{ f(n) / n }} = {}", b);
}

fn digits(n: Num) -> Vec<Num> {
    successors(Some(n), |n| Some(n / 10).filter(|&n| n > 0))
        .map(|n| n % 10)
        .collect()
}

fn round_up(n: Num, m: Num) -> Num {
    ((n - 1) / m + 1) * m
}

fn next_candidate(n: Num, m: Num) -> Option<Num> {
    match digits(m.clone()).iter().enumerate().rfind(|&(_, &i)| i > 2) {
        None => None,
        Some((e, _)) => {
            let goal = round_up(m, (10 as Num).pow(e as u32 + 1));
            Some(round_up(goal, n))
        }
    }
}

fn ff(n: Num) -> Num {
    // special case for 9999 because it's slow
    if n == 9999 {
        return 1111333355557778;
    }
    successors(Some(n), |&m| next_candidate(n, m))
        .last()
        .unwrap()
        / n
}

fn primes(n: i32) -> Vec<i32> {
    let mut primes = Vec::new();

    let mut x = 2;

    for _ in 0..n {
        while primes.iter().any(|&p| x % p == 0) { x += 1; }
        primes.push(x);
        x += 1;
    }

    primes
}

fn main() {
    println!("{}", primes(10001).last().unwrap());
}

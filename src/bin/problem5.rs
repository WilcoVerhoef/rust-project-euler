fn greatest_common_factor(a: i64, b: i64) -> i64 {
    let mut small: i64 = a.min(b);
    let mut great: i64 = a.max(b);

    while small > 0 {
        let new_small = great % small;
        great = small;
        small = new_small;
    }

    great
}

fn least_common_multiple(a: i64, b: i64) -> i64 {
    a * b / greatest_common_factor(a, b)
}

fn smallest_multiple(n: i64) -> i64 {
    let mut smallest = 1;

    for i in 1..n+1 {
        smallest = least_common_multiple(smallest, i);
    }

    smallest
}

fn main() {
    println!("{}", smallest_multiple(20));
}

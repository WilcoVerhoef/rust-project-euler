fn main() {
    println!("{}", h(3, 2));
}

fn b(n: i32) -> i32 {
    if n % 2 == 1 {
        1
    } else {
        2 * b(n / 2)
    }
}

fn a(n: i32) -> i32 {
    if n == 0 {
        1
    } else if n % 2 == 0 {
        3 * a(n / 2) + 5 * a(n - b(n / 2))
    } else {
        a(n / 2)
    }
}

fn h(t: u32, r: u32) -> i32 {
    let two: i32 = 2;
    a((two.pow(t) + 1).pow(r))
}

# rust-project-euler

For my first Rust programs I decided to solve some [Project Euler](https://projecteuler.net) problems.

Each problem has its own source file under [/src/bin](/src/bin).

# Running

1. [Install Rust](https://www.rust-lang.org/tools/install)
2. `cargo run --bin problem1`
